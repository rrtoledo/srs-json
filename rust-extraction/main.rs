extern crate pairing;
extern crate bellman;

use pairing::{CurveAffine, CurveProjective, GroupDecodingError, EncodedPoint};

use bellman::multicore::Worker;
use bellman::domain::{EvaluationDomain, Point};

use std::fs::File;
use std::fs::OpenOptions;
use std::io::{self, BufReader, BufWriter, Write, Read};

#[derive(Debug, Deserialize, Serialize)]
struct SRS {
    g1: Vec<Vec<u8>>,
    g2: Vec<Vec<u8>>,
}

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

/// Errors that might occur during deserialization.
#[derive(Debug)]
pub enum DeserializationError {
    IoError(io::Error),
    DecodingError(GroupDecodingError),
    PointAtInfinity
}

/// Checks if pairs have the same ratio.
fn same_ratio<G1: CurveAffine>(
    g1: (G1, G1),
    g2: (G1::Pair, G1::Pair)
) -> bool
{
    g1.0.pairing_with(&g2.1) == g1.1.pairing_with(&g2.0)
}

fn increment_g1<G1: CurveAffine>(
    g1_xi : G1,
    g1_xi1: G1,
    g2: G1::Pair,
    g2_x: G1::Pair) -> bool
{
    g1_xi.pairing_with(&g2_x) == g1_xi1.pairing_with(&g2)
}

fn increment_g2<G1: CurveAffine>(
    g1 : G1,
    g1_x: G1,
    g2_xi: G1::Pair,
    g2_xi1: G1::Pair) -> bool
{
    g1.pairing_with(&g2_xi1) == g1_x.pairing_with(&g2_xi)
}

fn check_srs_equality<G1: CurveAffine>(
    srs1: std::vec::Vec<G1>,
    srs2: std::vec::Vec<G1::Pair>) -> bool
{
    let mut check = true;

    // get generators and first power
    let g1 = srs1[0];
    let g2 = srs2[0];

    // equality test
    println!("\n-- Equality test --");
    for i in 0..std::cmp::min(srs1.len(), srs2.len()) {
        if !same_ratio( (g1, srs1[i]), (g2, srs2[i])) {
            println!("{} not equal", i);
            check = false;
        }
    }
    if check {
        println!("-- equality test passed.");
    }
    return check;
}

fn check_srs_increment<G1: CurveAffine>(
    srs1: std::vec::Vec<G1>,
    srs2: std::vec::Vec<G1::Pair>) -> bool
{
    let mut check_g1 = true;
    let mut check_g2 = true;

    // get generators and first power
    let g1 = srs1[0];
    let g1x = srs1[1];
    let g2 = srs2[0];
    let g2x = srs2[1];

    // increment tests
    println!("\n-- Increment tests --");
    print!("- G1:");
    for i in 0..(srs1.len()-1) {
        if !increment_g1(srs1[i], srs1[i+1], g2, g2x) {
            println!("g1^x^{} is not incremented", i);
            check_g1 = false
        }
    }
    if check_g1 {
        println!(" passed");
    } else {
        println!(" NOT passed");
    }

    print!("- G2:");
    for i in 0..(srs2.len()-1) {
        if !increment_g2(g1, g1x, srs2[i], srs2[i+1]) {
            println!("g2^x^{} is not incremented", i);
            check_g2 = false
        }
    }
    if check_g2 {
        println!(" passed");
    } else {
        println!(" NOT passed");
    }

    if check_g1 && check_g2 {
        println!("-- increment tests passed.");
    } else {
        println!("-- increment tests NOT passed");
    }

    return check_g1 && check_g2;
}

fn check_srs<G1: CurveAffine>(
    srs1: std::vec::Vec<G1>,
    srs2: std::vec::Vec<G1::Pair>) -> bool
{
    println!("\n----- Check all tests -----");
    // equality test
    let check_equality = check_srs_equality(srs1.to_vec(), srs2.to_vec());

    // increment tests
    let check_increment = check_srs_increment(srs1, srs2);
    
    if check_equality && check_increment {
        println!("\n----- all tests passed.\n");
    }
    return check_equality && check_increment;
}


fn read_uncompressed<C: CurveAffine, R: Read>(reader: &mut R) -> Result<C, DeserializationError> {
    let mut repr = C::Uncompressed::empty();
    reader.read_exact(repr.as_mut()).expect("Error reading file");
    let v = repr.into_affine().unwrap();

    if v.is_zero() {
        Err(DeserializationError::PointAtInfinity)
    } else {
        Ok(v)
    }
}

#[allow(dead_code)]
fn extract_powers_of_tau(exp: i8) {

    let mut m = 1;
    for _ in 1..exp+1 {
        m *= 2;
    }

    let worker = &Worker::new();

    // We first downloaded the ZCash powers of tau (https://download.z.cash/downloads/powersoftau/)
    // in a local folder "powersoftau"
    // Try to load "phase1radix2m{}"
    let f = match File::open(format!("../powersoftau/phase1radix2m{}", exp)) {
        Ok(f) => f,
        Err(e) => {
            panic!("Couldn't load phase1radix2m{}: {:?}", exp, e);
        }
    };
    let f = &mut BufReader::with_capacity(1024 * 1024, f);


    // Reading alpha_g1, beta_g1 and beta_g2
    let _ : pairing::bls12_381::G1Affine = read_uncompressed(f).expect("Error reading point g1"); 
    let _ : pairing::bls12_381::G1Affine = read_uncompressed(f).expect("Error reading point g1");
    let _ : pairing::bls12_381::G2Affine = read_uncompressed(f).expect("Error reading point g2");

    // Read the powers of tau written as Lagrange coefficients and convert into project coordinates
    let mut coeffs_g1_proj = Vec::with_capacity(m);
    for _ in 0..m {
        let coeff_g1_uncompressed : pairing::bls12_381::G1Affine = read_uncompressed(f).expect("Error reading point g1");
        let coeff_g1_uncompressed : bellman::domain::Point<pairing::bls12_381::G1> = Point(coeff_g1_uncompressed.into_projective());
        coeffs_g1_proj.push(coeff_g1_uncompressed);
    }

    let mut coeffs_g2_proj = Vec::with_capacity(m);
    for _ in 0..m {
        let coeff_g2_uncompressed : pairing::bls12_381::G2Affine = read_uncompressed(f).expect("Error reading point g2");
        let coeff_g2_uncompressed : bellman::domain::Point<pairing::bls12_381::G2> = Point(coeff_g2_uncompressed.into_projective());
        coeffs_g2_proj.push(coeff_g2_uncompressed);
    }

    // Convert all the Lagrange coefficients into powers of tau
    let mut coeffs_g1_proj = EvaluationDomain::from_coeffs(coeffs_g1_proj).unwrap();
    let mut coeffs_g2_proj = EvaluationDomain::from_coeffs(coeffs_g2_proj).unwrap();

    coeffs_g1_proj.fft(&worker);
    coeffs_g2_proj.fft(&worker);

    let coeffs_g1_powers = coeffs_g1_proj.into_coeffs();
    let coeffs_g2_powers = coeffs_g2_proj.into_coeffs();

    let coeffs_g1_powers = coeffs_g1_powers.into_iter()
                            .map(|e| e.0)
                            .collect::<Vec<_>>();

    let coeffs_g2_powers = coeffs_g2_powers.into_iter()
                            .map(|e| e.0)
                            .collect::<Vec<_>>();

    // Convert Powers of tau into affine coordinate for testing
    let mut coeffs_g1_affine = Vec::with_capacity(m);
    for coeff in coeffs_g1_powers {
        // Was normalized earlier in parallel
        let coeff = coeff.into_affine();
        coeffs_g1_affine.push(coeff);
    }
    let mut coeffs_g2_affine = Vec::with_capacity(m);
    for coeff in coeffs_g2_powers {
        let coeff = coeff.into_affine();
        coeffs_g2_affine.push(coeff);
    }

    println!("---------- Testing powers of tau ----------");
    check_srs(coeffs_g1_affine.to_vec(), coeffs_g2_affine.to_vec());

    // Convert Powers of tau into uncompressed format strings to export as JSON
    let mut coeffs_g1_string = Vec::with_capacity(m);
    for coeff in coeffs_g1_affine {
        let coeff = coeff.into_uncompressed().as_ref().to_vec();
        coeffs_g1_string.push(coeff);
    }
    let mut coeffs_g2_string = Vec::with_capacity(m);
    for coeff in coeffs_g2_affine {
        let coeff = coeff.into_uncompressed().as_ref().to_vec();
        coeffs_g2_string.push(coeff);
    }

    let coeffs_g1_verifier = coeffs_g1_string.to_vec();
    let mut coeffs_g2_verifier = coeffs_g2_string.to_vec();
    coeffs_g2_verifier.truncate(2);

    let input_prover = SRS { g1: coeffs_g1_string, g2: coeffs_g2_string };
    let json_prover = serde_json::to_string(&input_prover).unwrap();
    let filename_prover = format!("./json/SRS_prover_power{}.json", exp);
    let mut file_prover = File::create(filename_prover).unwrap();
    serde_json::to_writer_pretty(&mut file_prover, &json_prover).expect("Could not write JSON to file.");

    let input_verifier = SRS { g1: coeffs_g1_verifier, g2: coeffs_g2_verifier };
    let json_verifier = serde_json::to_string(&input_verifier).unwrap();
    let filename_verifier = format!("./json/SRS_verifier_power{}.json", exp);
    let mut file_verifier = File::create(filename_verifier).unwrap();
    serde_json::to_writer_pretty(&mut file_verifier, &json_verifier).expect("Could not write JSON to file.");

}

#[allow(dead_code)]
fn extract_lagrange_coeffs(exp: i8) {

    let mut m = 1;
    for _ in 1..exp+1 {
        m *= 2;
    }

    // We first downloaded the ZCash powers of tau (https://download.z.cash/downloads/powersoftau/)
    // in a local folder "powersoftau"
    // Try to load "phase1radix2m{}"
    let f = match File::open(format!("../powersoftau/phase1radix2m{}", exp)) {
        Ok(f) => f,
        Err(e) => {
            panic!("Couldn't load phase1radix2m{}: {:?}", exp, e);
        }
    };
    let f = &mut BufReader::with_capacity(1024 * 1024, f);

    // Reading alpha_g1, beta_g1 and beta_g2
    let _ : pairing::bls12_381::G1Affine = read_uncompressed(f).expect("Error reading point g1"); 
    let _ : pairing::bls12_381::G1Affine = read_uncompressed(f).expect("Error reading point g1");
    let _ : pairing::bls12_381::G2Affine = read_uncompressed(f).expect("Error reading point g2");

    // Read the powers of tau written as Lagrange coefficients and test for equality
    let mut coeffs_g1_affine = Vec::with_capacity(m);
    for _ in 0..m {
        let coeff_g1_uncompressed : pairing::bls12_381::G1Affine = read_uncompressed(f).expect("Error reading point g1");
        coeffs_g1_affine.push(coeff_g1_uncompressed);
    }

    let mut coeffs_g2_affine = Vec::with_capacity(m);
    for _ in 0..m {
        let coeff_g2_uncompressed : pairing::bls12_381::G2Affine = read_uncompressed(f).expect("Error reading point g2");
        coeffs_g2_affine.push(coeff_g2_uncompressed);
    }

    println!("---------- Testing Lagrange coeffs ----------");
    check_srs_equality(coeffs_g1_affine.to_vec(), coeffs_g2_affine.to_vec());

    // Convert Powers of tau into uncompressed format strings to export as JSON
    let mut coeffs_g1_string = Vec::with_capacity(m);
    for coeff in coeffs_g1_affine {
        let coeff = coeff.into_uncompressed().as_ref().to_vec();
        coeffs_g1_string.push(coeff);
    }
    let mut coeffs_g2_string = Vec::with_capacity(m);
    for coeff in coeffs_g2_affine {
        let coeff = coeff.into_uncompressed().as_ref().to_vec();
        coeffs_g2_string.push(coeff);
    }

    let g1_coeffs_verifier = coeffs_g1_string.to_vec();
    let mut g2_coeffs_verifier = coeffs_g2_string.to_vec();
    g2_coeffs_verifier.truncate(2);

    let input_prover = SRS { g1: coeffs_g1_string, g2: coeffs_g2_string };
    let json_prover = serde_json::to_string(&input_prover).unwrap();
    let filename_prover = format!("SRS_prover_lagrange_power{}.json", exp);
    let mut file_prover = File::create(filename_prover).unwrap();
    serde_json::to_writer_pretty(&mut file_prover, &json_prover).expect("Could not write JSON to file.");

    let input_verifier = SRS { g1: g1_coeffs_verifier, g2: g2_coeffs_verifier };
    let json_verifier = serde_json::to_string(&input_verifier).unwrap();
    let filename_verifier = format!("SRS_verifier_lagrange_power{}.json", exp);
    let mut file_verifier = File::create(filename_verifier).unwrap();
    serde_json::to_writer_pretty(&mut file_verifier, &json_verifier).expect("Could not write JSON to file.");
}


#[allow(dead_code)]
fn create_fft_test_vectors(exp: i8) {

    let mut m = 1;
    for _ in 1..exp+1 {
        m *= 2;
    }

    // We first downloaded the ZCash powers of tau (https://download.z.cash/downloads/powersoftau/)
    // in a local folder "powersoftau"
    // Try to load "phase1radix2m{}"
    let f = match File::open(format!("../powersoftau/phase1radix2m{}", exp)) {
        Ok(f) => f,
        Err(e) => {
            panic!("Couldn't load phase1radix2m{}: {:?}", exp, e);
        }
    };
    let f = &mut BufReader::with_capacity(1024 * 1024, f);

    let worker = &Worker::new();

    // Read the powers of tau written as Lagrange coefficients and convert into project coordinates
    let mut coeffs_g1_proj = Vec::with_capacity(m);
    for _i in 0..m {
        let coeff_g1_uncompressed : pairing::bls12_381::G1Affine = read_uncompressed(f).expect("Error reading point g1");
        let coeff_g1_uncompressed : bellman::domain::Point<pairing::bls12_381::G1> = Point(coeff_g1_uncompressed.into_projective());
        coeffs_g1_proj.push(coeff_g1_uncompressed);
    }

    let mut coeffs_g2_proj = Vec::with_capacity(m);
    for _j in 0..m {
        let coeff_g2_uncompressed : pairing::bls12_381::G2Affine = read_uncompressed(f).expect("Error reading point g2");
        let coeff_g2_uncompressed : bellman::domain::Point<pairing::bls12_381::G2> = Point(coeff_g2_uncompressed.into_projective());
        coeffs_g2_proj.push(coeff_g2_uncompressed);
    }

    // Convert all the Lagrange coefficients into powers of tau
    let mut coeffs_g1_proj = EvaluationDomain::from_coeffs(coeffs_g1_proj).unwrap();
    let mut coeffs_g2_proj = EvaluationDomain::from_coeffs(coeffs_g2_proj).unwrap();

    coeffs_g1_proj.fft(&worker);
    coeffs_g2_proj.fft(&worker);

    let coeffs_g1_powers = coeffs_g1_proj.into_coeffs();
    let coeffs_g2_powers = coeffs_g2_proj.into_coeffs();

    let coeffs_g1_powers = coeffs_g1_powers.into_iter()
                            .map(|e| e.0)
                            .collect::<Vec<_>>();

    let coeffs_g2_powers = coeffs_g2_powers.into_iter()
                            .map(|e| e.0)
                            .collect::<Vec<_>>();

    // Convert Powers of tau into affine coordinate for testing
    let mut coeffs_g1_affine = Vec::with_capacity(m);
    for coeff in coeffs_g1_powers {
        // Was normalized earlier in parallel
        let coeff = coeff.into_affine();
        coeffs_g1_affine.push(coeff);
    }
    let mut coeffs_g2_affine = Vec::with_capacity(m);
    for coeff in coeffs_g2_powers {
        let coeff = coeff.into_affine();
        coeffs_g2_affine.push(coeff);
    }

    let filename_g1 = format!("./test_vector/fft_test_vector_g1_{}", exp);
    let writer = OpenOptions::new()
                        .read(false)
                        .write(true)
                        .create_new(true)
                        .open(filename_g1)
                        .expect("unable to create parameter file in this directory");
    let mut writer = BufWriter::new(writer);
    for coeff in coeffs_g1_affine {
        writer.write_all(
            coeff.into_uncompressed()
                .as_ref()
        ).unwrap();
    }

    let filename_g2 = format!("./test_vector/fft_test_vector_g2_{}", exp);
    let writer = OpenOptions::new()
                        .read(false)
                        .write(true)
                        .create_new(true)
                        .open(filename_g2)
                        .expect("unable to create parameter file in this directory");
    let mut writer = BufWriter::new(writer);
    for coeff in coeffs_g2_affine {
        writer.write_all(
            coeff.into_uncompressed()
                .as_ref()
        ).unwrap();
    }    
}


#[allow(dead_code)]
fn create_ifft_test_vectors(exp: i8) {

    let mut m = 1;
    for _ in 1..exp+1 {
        m *= 2;
    }

    // We first downloaded the ZCash powers of tau (https://download.z.cash/downloads/powersoftau/)
    // in a local folder "powersoftau"
    // Try to load "phase1radix2m{}"
    let f = match File::open(format!("../powersoftau/phase1radix2m{}", exp)) {
        Ok(f) => f,
        Err(e) => {
            panic!("Couldn't load phase1radix2m{}: {:?}", exp, e);
        }
    };
    let f = &mut BufReader::with_capacity(1024 * 1024, f);

    let worker = &Worker::new();

    // Read the powers of tau written as Lagrange coefficients and convert into project coordinates
    let mut coeffs_g1_proj = Vec::with_capacity(m);
    for _ in 0..m {
        let coeff_g1_uncompressed : pairing::bls12_381::G1Affine = read_uncompressed(f).expect("Error reading point g1");
        let coeff_g1_uncompressed : bellman::domain::Point<pairing::bls12_381::G1> = Point(coeff_g1_uncompressed.into_projective());
        coeffs_g1_proj.push(coeff_g1_uncompressed);
    }

    let mut coeffs_g2_proj = Vec::with_capacity(m);
    for _ in 0..m {
        let coeff_g2_uncompressed : pairing::bls12_381::G2Affine = read_uncompressed(f).expect("Error reading point g2");
        let coeff_g2_uncompressed : bellman::domain::Point<pairing::bls12_381::G2> = Point(coeff_g2_uncompressed.into_projective());
        coeffs_g2_proj.push(coeff_g2_uncompressed);
    }

    // Convert all the Lagrange coefficients into powers of tau
    let mut coeffs_g1_proj = EvaluationDomain::from_coeffs(coeffs_g1_proj).unwrap();
    let mut coeffs_g2_proj = EvaluationDomain::from_coeffs(coeffs_g2_proj).unwrap();

    coeffs_g1_proj.ifft(&worker);
    coeffs_g2_proj.ifft(&worker);

    let coeffs_g1_powers = coeffs_g1_proj.into_coeffs();
    let coeffs_g2_powers = coeffs_g2_proj.into_coeffs();

    let coeffs_g1_powers = coeffs_g1_powers.into_iter()
                            .map(|e| e.0)
                            .collect::<Vec<_>>();

    let coeffs_g2_powers = coeffs_g2_powers.into_iter()
                            .map(|e| e.0)
                            .collect::<Vec<_>>();

    // Convert Powers of tau into affine coordinate for testing
    let mut coeffs_g1_affine = Vec::with_capacity(m);
    for coeff in coeffs_g1_powers {
        // Was normalized earlier in parallel
        let coeff = coeff.into_affine();
        coeffs_g1_affine.push(coeff);
    }
    let mut coeffs_g2_affine = Vec::with_capacity(m);
    for coeff in coeffs_g2_powers {
        let coeff = coeff.into_affine();
        coeffs_g2_affine.push(coeff);
    }
    let filename_g1 = format!("./test_vector/ifft_test_vector_g1_{}", exp);
    let writer = OpenOptions::new()
                        .read(false)
                        .write(true)
                        .create_new(true)
                        .open(filename_g1)
                        .expect("unable to create parameter file in this directory");
    let mut writer = BufWriter::new(writer);
    for coeff in coeffs_g1_affine {
        writer.write_all(
            coeff.into_uncompressed()
                .as_ref()
        ).unwrap();
    }

    let filename_g2 = format!("./test_vector/ifft_test_vector_g2_{}", exp);
    let writer = OpenOptions::new()
                        .read(false)
                        .write(true)
                        .create_new(true)
                        .open(filename_g2)
                        .expect("unable to create parameter file in this directory");
    let mut writer = BufWriter::new(writer);
    for coeff in coeffs_g2_affine {
        writer.write_all(
            coeff.into_uncompressed()
                .as_ref()
        ).unwrap();
    } 
}

#[allow(dead_code)]
fn main() {
    //create_fft_test_vectors(2);
    //create_ifft_test_vectors(2);
    extract_powers_of_tau(1);
    //extract_lagrange_coeffs(2);
}