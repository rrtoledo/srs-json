open Bls12_381


type srs = {
  g1: string list;
  g2: string list;
} [@@deriving to_yojson]

let rec power2 x =
  if x = 0 then 1 else 2 * power2(x-1);;

let equality_test srs = 
  let g1 = G1.Uncompressed.of_bytes_exn (Bytes.of_string (List.hd srs.g1)) in
  let g2 = G2.Uncompressed.of_bytes_exn (Bytes.of_string (List.hd srs.g2)) in 
  List.iteri (fun i _ ->
    let g1i = G1.Uncompressed.of_bytes_exn (Bytes.of_string (List.nth srs.g1 i)) in
    let g2i = G2.Uncompressed.of_bytes_exn (Bytes.of_string (List.nth srs.g2 i)) in
    let gt_element1 = Pairing.pairing g1i g2 in
    let gt_element2 = Pairing.pairing g1 g2i in
    assert (Fq12.eq gt_element1 gt_element2);
  ) srs.g2

let incrementation_g1_test srs =
  let g2 = G2.Uncompressed.of_bytes_exn (Bytes.of_string (List.hd srs.g2)) in 
  let g2x = G2.Uncompressed.of_bytes_exn (Bytes.of_string (List.nth srs.g2 1)) in
  List.iteri (fun i _ ->
    if i < (List.length srs.g1) - 1 then
      let g1i = G1.Uncompressed.of_bytes_exn (Bytes.of_string (List.nth srs.g1 i)) in
      let g1iplus = G1.Uncompressed.of_bytes_exn (Bytes.of_string (List.nth srs.g1 (i+1))) in
      let gt_element1 = Pairing.pairing g1i g2x in
      let gt_element2 = Pairing.pairing g1iplus g2 in
      assert (Fq12.eq gt_element1 gt_element2);
        ) srs.g1
  
let incrementation_g2_test srs = 
  let g1 = G1.Uncompressed.of_bytes_exn (Bytes.of_string (List.hd srs.g1)) in
  let g1x = G1.Uncompressed.of_bytes_exn (Bytes.of_string (List.nth srs.g1 1)) in
  List.iteri (fun i _ ->
    if i < (List.length srs.g2) - 1 then
      let g2i = G2.Uncompressed.of_bytes_exn (Bytes.of_string (List.nth srs.g2 i)) in
      let g2iplus = G2.Uncompressed.of_bytes_exn (Bytes.of_string (List.nth srs.g2 (i+1))) in
      let gt_element1 = Pairing.pairing g1x g2i in
      let gt_element2 = Pairing.pairing g1 g2iplus in
      assert (Fq12.eq gt_element1 gt_element2);
  ) srs.g2

let pairing_checks srs = 
  equality_test srs;
  incrementation_g1_test srs;
  incrementation_g2_test srs

let create_srs1 d x =
  let rec encoded_pow_x acc xi i =
    if i = 0 then List.rev acc
    else encoded_pow_x (xi :: acc) (G1.Uncompressed.mul xi x) (i - 1)
  in
  encoded_pow_x [] G1.Uncompressed.one d

let create_srs2 d x =
  let rec encoded_pow_x acc xi i =
    if i = 0 then List.rev acc
    else encoded_pow_x (xi :: acc) (G2.Uncompressed.mul xi x) (i - 1)
  in
  encoded_pow_x [] G2.Uncompressed.one d

  let srs1_to_string srs1 =
    List.init (List.length srs1) (fun i -> 
      Bytes.to_string (G1.Uncompressed.to_bytes (List.nth srs1 i) )
    ) 

  let srs2_to_string srs2 =
    List.init (List.length srs2) (fun i -> 
      Bytes.to_string (G2.Uncompressed.to_bytes (List.nth srs2 i) )
    )

let test_check power =
  let m = power2 power in
  let x = Fr.random () in
  let l1 = srs1_to_string (create_srs1 m x) in
  let l2 = srs2_to_string (create_srs2 m x) in
  let srs_random = {g1= l1; g2= l2} in
  pairing_checks srs_random

let rec powerof2 x =
  if x = 0 then 1 else 2 * powerof2(x-1)

let rec int_of_bin l =
  match l with
  | [] -> 0
  | x :: sl -> if x=1 then powerof2 (List.length sl) + int_of_bin sl else int_of_bin sl
  
let bin_of_int x =
  let rec bof y lst = match y with
    | 0 -> lst
    | _ -> bof (y/2) ((y mod 2) :: lst)
  in
  let binary = bof x [] in
  (List.init (32- (List.length binary)) (fun _ -> 0)) @ binary

let bit_reverse n size =
  let binary = bin_of_int n in
  let reversed = List.rev binary in
  let slist = Array.to_list (Array.sub (Array.of_list reversed) 0 size) in
  int_of_bin slist

let rec fft_inner_for_loop values omega w_m w m k j gstring gmul gadd gneg =
  if j < m then
    let t = Array.get values (k+j+m) in
    let temp = Array.get values (k+j) in
    let t1 = gmul t w in
    let t2 = gadd temp (gneg t1) in
    Array.set values (k+j+m) t2;
    Array.set values (k+j) (gadd (Array.get values (k+j)) t1);
    let w = Fr.mul w w_m in
    fft_inner_for_loop values omega w_m w m k (j+1) gstring gmul gadd gneg

let rec fft_inner_while_loop values omega w_m m k gstring gmul gadd gneg =
  if k < (Array.length values) then
    let w = Fr.one in
    fft_inner_for_loop values omega w_m w m k 0 gstring gmul gadd gneg;
    let k = k + 2*m in
    fft_inner_while_loop values omega w_m m k gstring gmul gadd gneg

let rec fft_outer_for_loop values omega m log_n gstring gmul gadd gneg =
  if  log_n > 0 then
    let exp = string_of_int ( (Array.length values) / (2*m)) in
    let w_m = Fr.pow omega (Z.of_string exp) in
    fft_inner_while_loop values omega w_m m 0 gstring gmul gadd gneg;
    let m = 2*m in
    fft_outer_for_loop values omega m (log_n-1) gstring gmul gadd gneg
  else
    let values = values in
    values

(* Output the correct root of unity*)
let rec get_omega limit = 
  if limit < 32 then 
    Fr.square (get_omega (limit+1))
  else
    Fr.of_string "0x16a2a19edfe81f20d09b681922c813b4b63683508c2280b93829971f439f0d2b"

let log2 x =
  let m = 1 in
  let exp = 0 in
  let rec loop m exp x =
    if m < List.length x then loop (2*m) (exp+1) x else m, exp in
  loop m exp x
  

(* FFT for list of group elements*)
let fft_group values gstring gzero gmul gadd gneg =
  let m, log_n = log2 values in
  let omega =  get_omega log_n in
  let values = Array.init m ( fun i ->
    if i < (List.length values) then Array.get (Array.of_list values) i else gzero) in
  Array.iteri (fun i _ ->
    let j = bit_reverse i log_n in
    if i < j then
      (* swap i and j*)
      let x = Array.get values i in
      let y = Array.get values j in
      Array.set values i y;
      Array.set values j x;

  ) values;
  fft_outer_for_loop values omega 1 log_n gstring gmul gadd gneg


(* Read bytes from file and convert to element of G1, raise exception on failure*)
let read_g1 ic =
  let bytes_buf = Bytes.create G1.Uncompressed.size_in_bytes in
  Stdlib.really_input ic bytes_buf 0 G1.Uncompressed.size_in_bytes; 
  let check_group_element = G1.Uncompressed.of_bytes_exn bytes_buf in
  Bytes.to_string (G1.Uncompressed.to_bytes check_group_element)

(* Read bytes from file and convert to element of G2, raise exception on failure*)
let read_g2 ic = 
  let bytes_buf = Bytes.create G2.Uncompressed.size_in_bytes in
    Stdlib.really_input ic bytes_buf 0 G2.Uncompressed.size_in_bytes;
    let check_group_element = G2.Uncompressed.of_bytes_exn bytes_buf in
    Bytes.to_string (G2.Uncompressed.to_bytes check_group_element)


  let path_power f =
    let current_dir = Sys.getcwd () in
    let project_dir_index = Str.search_forward (Str.regexp_string "srs-json") current_dir 0 in
    let project_dir = String.sub current_dir 0 (project_dir_index + (String.length "srs-json")) in
    project_dir ^ "/powersoftau/"
    

(* Extract all values from radix file*)
let extract_all_values power = 
  let filename = path_power ("phase1radix2m" ^ (string_of_int power)) in
  let ic = open_in_bin filename in
  (* Computing number of elements *)
  let m = power2 power in
  (*alpha*)
  let _ = read_g1 ic in
  (*beta g1*)
  let _ = read_g1 ic in
  (*beta g2*)
  let _ = read_g2 ic in
  (*lagrange coeffs in g1*)
  let coeffs_g1 = List.init m (fun _ -> read_g1 ic) in
  (*lagrange coeffs in g2*)
  let coeffs_g2 = List.init m (fun _ -> read_g2 ic) in
  (*alpha coeffs g1*)
  let _ = List.init m (fun _ -> read_g1 ic) in
  (*beta coeffs g1*)
  let _ = List.init m (fun _ -> read_g1 ic) in
  (*h*)
  let _ = List.init (m-1) (fun _ -> read_g1 ic) in
  let srs_plonk = {g1=coeffs_g1; g2=coeffs_g2} in
  pairing_checks srs_plonk
  

let extract_srs power prover powers_of_tau= 
  let filename = path_power ("phase1radix2m" ^ (string_of_int power)) in
  let ic = open_in_bin filename in
  (* Computing number of elements *)
  let m = power2 power in
  let bytes = Bytes.create (2*G1.Uncompressed.size_in_bytes + G2.Uncompressed.size_in_bytes) in
  Stdlib.really_input ic bytes 0 (2*G1.Uncompressed.size_in_bytes + G2.Uncompressed.size_in_bytes);
  (* Computing index of first power of tau,
    there are alpha**g1, beta**g1 and beta**g1
    before the powers of tau *)
  let g1_elements = List.init m (fun _i -> 
    let bytes_buf = Bytes.create G1.Uncompressed.size_in_bytes in
    Stdlib.really_input ic bytes_buf 0 G1.Uncompressed.size_in_bytes; 
    G1.Uncompressed.of_bytes_exn bytes_buf) in
  (* Retrieve g2, x**G2  *)
  let g2_elements = List.init m (fun _i -> 
    let bytes_buf = Bytes.create G2.Uncompressed.size_in_bytes in
    Stdlib.really_input ic bytes_buf 0 G2.Uncompressed.size_in_bytes;
    G2.Uncompressed.of_bytes_exn bytes_buf) in
  close_in ic;

  let g1_elements = if powers_of_tau then (fft_group g1_elements G1.Uncompressed.to_bytes G1.Uncompressed.zero G1.Uncompressed.mul G1.Uncompressed.add G1.Uncompressed.negate) else Array.of_list g1_elements in
  let g2_elements = if powers_of_tau then (fft_group g2_elements G2.Uncompressed.to_bytes G2.Uncompressed.zero G2.Uncompressed.mul G2.Uncompressed.add G2.Uncompressed.negate) else Array.of_list g2_elements in
  let nb_g2 = if prover then (Array.length g1_elements) else 2 in
  let g1_elements = List.init (Array.length g1_elements) (fun i -> Bytes.to_string (G1.Uncompressed.to_bytes (Array.get g1_elements i)) ) in
  let g2_elements = List.init nb_g2 (fun i -> Bytes.to_string (G2.Uncompressed.to_bytes (Array.get g2_elements i)) ) in
  let srs_plonk = {g1=g1_elements; g2=g2_elements} in
  srs_plonk


let path_json f =
  let current_dir = Sys.getcwd () in
  let project_dir_index = Str.search_forward (Str.regexp_string "srs-json") current_dir 0 in
  let project_dir = String.sub current_dir 0 (project_dir_index + (String.length "srs-json")) in
  project_dir ^ "/ocaml-extraction/json/" ^ f
  

  let extract_verifier_to_json power = 
  let srs = extract_srs power false true in
  pairing_checks srs;
  let filename = path_json  ("srs_verifier_power" ^ (string_of_int power) ^ ".json") in 
  let oc = open_out_bin filename in 
  Yojson.Safe.pretty_to_channel oc (srs_to_yojson {g1 = srs.g1; g2 = srs.g2});
  close_out oc;;

let extract_prover_to_json power = 
  let srs = extract_srs power true true in
  pairing_checks srs;
  let filename = path_json ("srs_prover_power" ^ (string_of_int power) ^ ".json") in
  let oc = open_out_bin filename in 
  Yojson.Safe.pretty_to_channel oc (srs_to_yojson {g1 = srs.g1; g2 = srs.g2});
  close_out oc;;

let extract_verifier_lagrange_to_json power = 
  let srs = extract_srs power false false in
  equality_test srs;
  let filename = path_json ("srs_verifier_lagrange_power" ^ (string_of_int power) ^ ".json") in 
  let oc = open_out_bin filename in 
  Yojson.Safe.pretty_to_channel oc (srs_to_yojson {g1 = srs.g1; g2 = srs.g2});
  close_out oc;;

let extract_prover_lagrange_to_json power = 
  let srs = extract_srs power true false in
  equality_test srs;
  let filename = path_json ("srs_prover_lagrange_power" ^ (string_of_int power) ^ ".json") in
  let oc = open_out_bin filename in 
  Yojson.Safe.pretty_to_channel oc (srs_to_yojson {g1 = srs.g1; g2 = srs.g2});
  close_out oc;;


type srs_code = {
  g1_code: int list list;
  g2_code: int list list;
} [@@deriving of_yojson]
  

let intlist_to_bytes x = 
  let res = Bytes.create (List.length x) in
  Bytes.iteri (fun i _ -> 
    let code = List.nth x i in
    Bytes.set_uint8 res i code 
    ) res;
  res

let path_test_vector f =
  let current_dir = Sys.getcwd () in
  let project_dir_index = Str.search_forward (Str.regexp_string "srs-json") current_dir 0 in
  let project_dir = String.sub current_dir 0 (project_dir_index + (String.length "srs-json")) in
  project_dir ^ "/ocaml-extraction/test-vector/" ^ f

let write_random_bytes len =
  let g1_elements = List.init len (fun _ -> G1.Uncompressed.to_bytes (G1.Uncompressed.random ())) in
  let g2_elements = List.init len (fun _ -> G2.Uncompressed.to_bytes (G2.Uncompressed.random ())) in
  let filename = path_test_vector  ("test_vector_g1_" ^ (string_of_int len)) in
  let oc = open_out_bin filename in 
  List.iter (fun x -> Printf.fprintf oc "%s" (Bytes.to_string x) ) g1_elements;
  close_out oc;
  let filename = path_test_vector ("test_vector_g2_" ^ (string_of_int len)) in
  let oc = open_out_bin filename in 
  List.iter (fun x -> Printf.fprintf oc "%s" (Bytes.to_string x) ) g2_elements;
  close_out oc;
  print_endline "success"
