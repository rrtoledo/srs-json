open Extraction

let () =
  let power = 5 in
  extract_prover_to_json power;
  extract_verifier_to_json power