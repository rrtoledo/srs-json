#!/bin/bash

re='^[0-9]+$'
if [ "$#" -ne 1 ]; then
	echo "Expected one argument"
	exit 1
else
	if [ "$1" != "all" ] && ! [[ "$1" =~ $re ]]; then
		echo Expected \'all\' or number.
		exit 1
	fi
fi

# Enter directory
power_folder='./powers-of-tau'
if [ ! -d "$power_folder" ]; then
	mkdir "$power_folder"
fi
cd "$power_folder"

# Download hash list if needs be
sha_file='SHA256SUMS.txt'
tmp_file='tmp'
sha_url='https://zcash.dankdoggo.net/transcripts/SHA256SUMS.txt'
if ! test -f "$sha_file"; then
	wget --no-check-certificate -q "$sha_url"
	(sed '26,$d' $sha_file) > "$tmp_file"
	(sed '1,3d' $tmp_file) > "$sha_file"
	(cut -d' ' -f1 < "$sha_file") > "$tmp_file"
	mv "$tmp_file" "$sha_file"
fi

# Download powers of tau
# TODO stop script when nothing was downloaded
power_url="https://download.z.cash/downloads/powersoftau/"
prefix="phase1radix2m"
if [ "$1" = "all" ]; then
	for i in $(seq 0 21); do
		wget -nc -q "$power_url$prefix$i"
	done
else
	wget -nc -q "$power_url$prefix$1"
fi

# Check hashes of all powers of tau
for file in $(ls | grep "$prefix"); do
	nb=${file#"$prefix"}
	local_sha=$(sha256sum "phase1radix2m$nb" | cut -d' ' -f1)
	expected_sha=$(sed "$(($nb+1))q;d" "$sha_file")

	if [ "$local_sha" = "$expected_sha" ]; then
		continue
	else
		echo Hash assertion fail for: "phase1radix2m$nb"
		echo computed sha is $local_sha
		echo we expected $expected_sha
		exit 1
	fi
done

# Extract srs from powers of tau
cd ..

srs_folder="srs"
if [ ! -d "$srs_folder" ]; then
	mkdir "$srs_folder"
fi
for file in $(ls "$power_folder" | grep "$prefix"); do
	nb=${file#"$prefix"}
	if ! test -f "$srs_folder/srs_$nb"; then
		dune exec exec/generate_srs_"$nb"
	fi
done
