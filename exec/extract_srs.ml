module G1 = Bls12_381.G1.Uncompressed
module G2 = Bls12_381.G2.Uncompressed
module Fr = Bls12_381.Fr

let generate_domain power size =
  let rec get_omega limit =
    if limit < 32 then Fr.square (get_omega (limit + 1))
    else
      Fr.of_string
        "0x16a2a19edfe81f20d09b681922c813b4b63683508c2280b93829971f439f0d2b"
  in
  let omega = get_omega power in
  let rec encoded_pow_x acc xi i =
    if i = 0 then List.rev acc
    else encoded_pow_x (xi :: acc) (Fr.mul xi omega) (i - 1)
  in
  encoded_pow_x [] Fr.one size

let extract_srs power input_file output_file =
  let ic = open_in_bin input_file in
  (* Computing number of elements *)
  let m = 1 lsl power in
  (* Reading alpha**g1, beta**g1 and beta**g1 which precedes the power of tau*)
  let bin_buff = Bytes.create ((2 * G1.size_in_bytes) + G2.size_in_bytes) in
  Stdlib.really_input ic bin_buff 0 ((2 * G1.size_in_bytes) + G2.size_in_bytes);
  (* Retrieve (x**i)**g1 *)
  let g1_elements =
    List.init m (fun _i ->
        let bytes_buf = Bytes.create G1.size_in_bytes in
        Stdlib.really_input ic bytes_buf 0 G1.size_in_bytes;
        G1.of_bytes_exn bytes_buf)
  in
  (* Retrieve (x**i)**g2 *)
  let g2_elements =
    List.init m (fun _i ->
        let bytes_buf = Bytes.create G2.size_in_bytes in
        Stdlib.really_input ic bytes_buf 0 G2.size_in_bytes;
        G2.of_bytes_exn bytes_buf)
  in
  close_in ic;
  let domain = generate_domain power m in
  let g1_elements = G1.fft ~domain ~points:g1_elements in
  let g2_elements = G2.fft ~domain ~points:g2_elements in
  let oc = open_out_bin output_file in
  output_bytes oc (G2.to_bytes (List.hd g2_elements));
  output_bytes oc (G2.to_bytes (List.nth g2_elements 1));
  List.iter (fun g1 -> output_bytes oc (G1.to_bytes g1)) g1_elements;
  close_out oc

let () =
  let pw = int_of_string Sys.argv.(1) in
  let input_file = Sys.argv.(2) in
  let output_file = Sys.argv.(3) in
  extract_srs pw input_file output_file
